package de.bixilon.bixpack.v4.bungeecore.classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Chat {

	public static void chat(ProxiedPlayer p, String message) {
		if (!Player.authenticated.contains(p.getUniqueId())) {
			p.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_USAGE));
			return;
		}

		if (!checkMessage(message))
			return;

		message = format(message);

		String prefix = "";

		Player pi = Main.mysql.getPlayerByUsername(p.getDisplayName());
		if (pi != null) {

			pi.setProxiedPlayer(p);
			BungeeCord.getInstance().broadcast(Strings.getTextComponent(Strings.CHAT_FORMAT,
					new String[] { prefix, Main.getColoredName(p), message }));

		} else
			p.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_USAGE));
	}

	public static boolean checkMessage(String message) {
		// checks if message does not have "bad" words in it.

		// returns true. feature not implemented yet
		return true;
	}

	public static String format(String message) {
		// Formats the message: Highlight online Players, remove color codes...

		// remove all color codes
		message = message.replace("§", "&");

		// Highlight player names

		Pattern p = Pattern.compile(Pattern.quote("@") + "(.*?)" + Pattern.quote(" "));
		Matcher m = p.matcher(message);
		while (m.find()) {
			ProxiedPlayer pp = Main.getProxiedPlayerByDisplayname(m.group(1));

			if (pp != null) {
				message = message.replace("@" + m.group(1), "§a@" + pp.getName() + "§f");
			}

		}

		// Now: if the Message ends with an playername it does not get regionices
		// because an [space] is missing. Looking up....

		int lastindexof = message.lastIndexOf("@") - 2;
		if (lastindexof <= 2) {
			lastindexof = 0;
		}
		if (message.charAt(lastindexof) != '§') {
			// condition true, and not color code is set. setting 1
			ProxiedPlayer pp = Main
					.getProxiedPlayerByDisplayname(message.substring(message.lastIndexOf("@") + 1, message.length()));

			if (pp != null) {
				// player is online
				message = message.replace("@" + message.substring(message.lastIndexOf("@") + 1, message.length()),
						"§a@" + pp.getName());

			}
		}

		return message;

	}

}
