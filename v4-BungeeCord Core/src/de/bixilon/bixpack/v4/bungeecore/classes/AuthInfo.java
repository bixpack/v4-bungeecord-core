package de.bixilon.bixpack.v4.bungeecore.classes;

public class AuthInfo {
	// contains original uuid and fake player uuid
	String name;
	long time;

	public AuthInfo(String name, long time) {
		this.name = name;
		this.time = time;
	}

	public long getAuthTime() {
		return time;
	}

	public String getFakeName() {
		return name;
	}
}
