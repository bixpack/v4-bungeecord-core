package de.bixilon.bixpack.v4.bungeecore.classes;

public class Util {
	public static boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static String yesorno(boolean to) {
		if (to)
			return "§aJa";
		return "§cNein";
	}
}
