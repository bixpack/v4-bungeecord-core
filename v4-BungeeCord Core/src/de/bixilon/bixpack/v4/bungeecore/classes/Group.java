package de.bixilon.bixpack.v4.bungeecore.classes;

import java.util.List;

import de.bixilon.bixpack.v4.bungeecore.main.Main;
import net.md_5.bungee.api.score.Team;

public class Group {

	int id;
	String name;
	String prefix;
	String suffix;

	List<String> permissions;

	public static List<Group> groups;
	public Team scoreboadteam;

	public Group(int id, String name, String prefix, String suffix) {
		this.id = id;
		this.name = name;
		this.prefix = prefix;
		this.suffix = suffix;
		permissions = Main.mysql.getPermissions(id);
		scoreboadteam = new Team(name);
		scoreboadteam.setPrefix(prefix);
		scoreboadteam.setSuffix(suffix);
	}

	public String getName() {
		return name;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public int getID() {
		return id;
	}

	public List<String> getPermissions() {
		return permissions;
	}

}
