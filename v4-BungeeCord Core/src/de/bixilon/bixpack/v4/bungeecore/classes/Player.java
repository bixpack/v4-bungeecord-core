package de.bixilon.bixpack.v4.bungeecore.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.connection.InitialHandler;
import net.md_5.bungee.connection.LoginResult;
import net.md_5.bungee.protocol.packet.LoginRequest;

public class Player {
	// fake player class

	public static List<UUID> authenticated = new ArrayList<UUID>();

	ProxiedPlayer pp;
	UUID fakeuuid;
	String name;
	Group g;

	String token;
	boolean origin;
	String server;

	boolean isauthentificated = false;

	public Player(ProxiedPlayer pp, UUID fakeuuid, String name, int group, String token, boolean origin,
			String server) {
		this.pp = pp;
		this.fakeuuid = fakeuuid;
		this.name = name;
		this.g = Group.groups.get(group - 1);
		this.token = token;
		this.origin = origin;
		this.server = server;
	}

	public boolean isAuthentificated() {
		if (authenticated.contains(pp.getUniqueId()))
			isauthentificated = true;
		return isauthentificated;
	}

	public void setAuthentificated(boolean state) {
		isauthentificated = state;
		authenticated.add(pp.getUniqueId());
	}

	public void init() {
		if (!origin) {

			// sets fake player uuid

			PendingConnection pC = pp.getPendingConnection();
			InitialHandler iC = (InitialHandler) pp.getPendingConnection();

			Class<?> pcClass = pC.getClass();
			Class<?> bungeeClass = BungeeCord.getInstance().getClass();
			Class<?> icClass = iC.getClass();
			java.lang.reflect.Field field;
			java.lang.reflect.Field field_f;
			try {

				// Player connection online uuid
				field = pcClass.getDeclaredField("uniqueId");
				field.setAccessible(true);
				field.set(pC, fakeuuid);
				// Player connection offline uuid
				field = pcClass.getDeclaredField("offlineId");
				field.setAccessible(true);
				field.set(pC, fakeuuid);

				// initial handler online uuid
				field = icClass.getDeclaredField("uniqueId");
				field.setAccessible(true); 
				field.set(iC, fakeuuid);
				// initial handler offline uuid
				field = icClass.getDeclaredField("offlineId");
				field.setAccessible(true);
				field.set(iC, fakeuuid);

				// BungeeCord player list(All ProxiedPlayers) by Name
				field = bungeeClass.getDeclaredField("connections");
				field.setAccessible(true);
				@SuppressWarnings("unchecked")
				UserConnection uC = ((Map<String, UserConnection>) field.get(BungeeCord.getInstance()))
						.get(pp.getName());

				BungeeCord.getInstance().removeConnection(uC);

				// fake Name for proxiedplayer
				Class<?> ucClass = uC.getClass();
				field_f = ucClass.getDeclaredField("name");
				field_f.setAccessible(true);
				field_f.set(uC, name);

				// save fake name
				BungeeCord.getInstance().addConnection(uC);

				// setting pending connection fake name; function: unknown
				field = pcClass.getDeclaredField("name");
				field.setAccessible(true);
				field.set(pC, name);

				// setting fake name for the pending connection; function: unknown
				field = pcClass.getDeclaredField("loginProfile");
				field.setAccessible(true);
				LoginResult lr = (LoginResult) field.get(pC);
				lr.setName(name);
				field.set(pC, lr);

				// setting pending connection fake name for the login request; function: unknown
				field = pcClass.getDeclaredField("loginRequest");
				field.setAccessible(true);
				LoginRequest lre = (LoginRequest) field.get(pC);
				lre.setData(name);
				field.set(pC, lre);

			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			pp.setDisplayName(name);
		}
	}

	public ProxiedPlayer getProxiedPlayer() {
		return pp;
	}

	public UUID getUUID() {
		return fakeuuid;
	}

	public String getServer() {
		return server;
	}

	public String getPlayerName() {
		return name;
	}

	public String getToken() {
		return token;
	}

	public void setProxiedPlayer(ProxiedPlayer pp) {
		this.pp = pp;

	}

	public Group getGroup() {
		return g;
	}

	public void setPermissions() {
		BungeeCord.getInstance().getLogger().info("Setting Permissions for " + pp.getDisplayName());
		for (int i = 0; i < getGroup().getPermissions().size(); i++) {
			String permission = getGroup().getPermissions().get(i);
			boolean state = true;
			if (permission.startsWith("-")) {
				permission = permission.substring(1, permission.length());
				state = false;
			}
			pp.setPermission(permission, state);
		}

	}
}
