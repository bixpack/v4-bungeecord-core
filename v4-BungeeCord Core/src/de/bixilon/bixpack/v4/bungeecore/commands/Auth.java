package de.bixilon.bixpack.v4.bungeecore.commands;

import java.security.GeneralSecurityException;

import de.bixilon.bixpack.v4.bungeecore.classes.AuthInfo;
import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import de.bixilon.bixpack.v4.bungeecore.classes.SecondFactorAuth;
import de.bixilon.bixpack.v4.bungeecore.classes.Util;
import de.bixilon.bixpack.v4.bungeecore.events.PostAuth;
import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Auth extends Command {
	public Auth() {
		super("login");
	}

	public void execute(CommandSender cs, String[] args) {
		Player p = null;

		if (cs.hasPermission("admin.auth")) {
			// User is admin. Can authenticate other players...
			if (args.length == 2) {
				ProxiedPlayer pp = BungeeCord.getInstance().getPlayer(args[0]);
				if (pp == null) {
					cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
				} else {
					p = Main.mysql.getPlayerByUsername(args[1]);
					if (p != null) {
						p.setProxiedPlayer(pp);
						auth(pp, p);
						pp.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_BY_ADMIN));
						cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_TO_ADMIN));
					} else
						cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_USER_NOT_FOUND));

				}

			} else
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_USAGE_ADMIN));

		} else {
			ProxiedPlayer ppp = BungeeCord.getInstance().getPlayer(cs.getName());
			if (Player.authenticated.contains(ppp.getUniqueId())) {
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_ALREADY_VERIFIED));
				return;
			}

			if (args.length == 2) {
				p = Main.mysql.getPlayerByUsername(args[0].replace('\'', ' ').replace('"', ' '));

				if (p != null) {
					try {
						if (!Util.isNumeric(args[1])) {
							ppp.disconnect(Strings.getTextComponent(Strings.COMMAND_AUTH_FAILED));
							return;
						}
						if (SecondFactorAuth.validateCurrentNumber(p.getToken(), Integer.valueOf(args[1]), 2000)) {
							p.setProxiedPlayer(ppp);
							auth(ppp, p);
						} else {
							ppp.disconnect(Strings.getTextComponent(Strings.COMMAND_AUTH_FAILED));
						}
					} catch (NumberFormatException | GeneralSecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else
					ppp.disconnect(Strings.getTextComponent(Strings.COMMAND_AUTH_USER_NOT_FOUND));

			} else
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_USAGE));
		}
	}

	private void auth(ProxiedPlayer pp, Player p) {
		PostAuth.auths.put(pp.getUniqueId(), new AuthInfo(p.getPlayerName(), (System.currentTimeMillis() / 1000)));
		pp.disconnect(Strings.getTextComponent(Strings.COMMAND_AUTH_REJOIN));
	}
}
