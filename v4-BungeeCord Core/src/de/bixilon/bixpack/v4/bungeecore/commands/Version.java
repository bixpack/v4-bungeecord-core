package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Version extends Command {
	public Version() {
		super("version");
	}

	public void execute(CommandSender cs, String[] args) {
		cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_VERSION));
		if (cs instanceof ProxiedPlayer) {
			((ProxiedPlayer) cs).chat("/version");
		}
	}
}
