package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Kick extends Command {
	public Kick() {
		super("kick");
	}

	public void execute(CommandSender cs, String[] args) {
		if (cs.hasPermission("admin.kick")) {
			if (args.length == 0)
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_KICK_USAGE));
			else if (args.length > 0) {
				ProxiedPlayer p = Main.getProxiedPlayerByDisplayname(args[0]);
				if (p != null) {

					if (args.length == 1) {
						// Kick Player for *no* reason!
						p.disconnect(Strings.getTextComponent(Strings.COMMAND_KICK_DISCONNECT,
								new String[] { cs.getName() }));
					} else {
						String message = "";
						for (int i = 1; i < args.length; i++) {
							message += args[i] + " ";
						}
						p.disconnect(Strings.getTextComponent(Strings.COMMAND_KICK_DISCONNECT_REASON,
								new String[] { cs.getName(), message }));
					}
				} else
					cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
			}

		} else
			cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_NO_PERMISSIONS));
	}
}
