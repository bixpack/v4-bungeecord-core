package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import de.bixilon.bixpack.v4.bungeecore.classes.Util;
import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Info extends Command {
	public Info() {
		super("info");
	}

	public void execute(CommandSender cs, String[] args) {
		if (cs instanceof ProxiedPlayer) {
			ProxiedPlayer p;
			if (args.length == 0)
				p = BungeeCord.getInstance().getPlayer(cs.getName());
			else {
				if (args[0].endsWith("_f"))
					p = Main.getProxiedPlayerByDisplayname(args[0]);
				else
					p = BungeeCord.getInstance().getPlayer(args[0]);
			}

			if (p != null) {
				Player pi = Main.mysql.getPlayerByUsername(p.getDisplayName());
				cs.sendMessage(new TextComponent("§c-------Spielerinfo:-------"));
				cs.sendMessage(new TextComponent("§6Onlinestatus: §aOnline"));
				cs.sendMessage(new TextComponent("§7Originalname: §f" + p.getName()));
				if (pi != null) {
					pi.setProxiedPlayer(p);
					cs.sendMessage(new TextComponent("§6Spielername: §f" + p.getDisplayName()));
					cs.sendMessage(new TextComponent("§6Anzeigename: " + Main.getColoredName(p)));
					cs.sendMessage(new TextComponent("§7Gruppe: " + pi.getGroup().getPrefix() + pi.getGroup().getName()
							+ pi.getGroup().getSuffix()));

					cs.sendMessage(new TextComponent("§6Authentifiziert: " + Util.yesorno(pi.isAuthentificated())));

				} else {
					cs.sendMessage(new TextComponent("§6Spielername: §f" + "§fUnbekannt"));
					cs.sendMessage(new TextComponent("§6Anzeigename: " + "§fUnbekannt"));
					cs.sendMessage(new TextComponent("§7Gruppe: " + "§fUnbekannt"));

					cs.sendMessage(new TextComponent("§6Authentifiziert: " + Util.yesorno(false)));

				}
				cs.sendMessage(new TextComponent("§7UUID: §f" + p.getUniqueId()));
				cs.sendMessage(new TextComponent("§6Server: §f" + p.getServer().getInfo().getName()));
				cs.sendMessage(new TextComponent("§7Ping: §f" + p.getPing() + "ms"));

			} else
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
		}
	}
}
