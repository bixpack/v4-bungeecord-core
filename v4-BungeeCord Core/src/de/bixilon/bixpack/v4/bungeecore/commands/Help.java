package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Help extends Command {
	public Help() {
		super("help");
	}

	public void execute(CommandSender cs, String[] args) {
		cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_HELP_TEXT));
		if (cs.hasPermission("admin.help")) {
			// User is an admin. Showing more....
			cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_HELP_TEXT_ADMIN));
		}
	}
}
