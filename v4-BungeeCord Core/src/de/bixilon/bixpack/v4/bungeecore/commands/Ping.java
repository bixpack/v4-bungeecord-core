package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Ping extends Command {
	public Ping() {
		super("ping");
	}

	public void execute(CommandSender cs, String[] args) {
		if (cs instanceof ProxiedPlayer) {
			if (args.length == 0) {
				ProxiedPlayer p = BungeeCord.getInstance().getPlayer(cs.getName());
				// First Bungeecord Ping

				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_PING_BUNGEE_SINGLE,
						new String[] { String.valueOf(p.getPing()) }));
				// Server Ping
				p.chat("/ping");
			} else if (args.length == 1) {
				ProxiedPlayer p = Main.getProxiedPlayerByDisplayname(args[0]);
				if (p != null) {
					cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_PING_BUNGEE_SINGLE,
							new String[] { String.valueOf(p.getPing()), Main.getColoredName(p) }));
					// Server Ping
					p.chat("/ping " + args[0]);
				} else
					cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
			} else
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_PING_USAGE));

		} else
			cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_CONSOLE_NO_ALLOWED));

	}
}
