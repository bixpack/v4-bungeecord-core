package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Sync extends Command {
	public Sync() {
		super("sync");
	}

	public void execute(CommandSender cs, String[] args) {
		if (cs.hasPermission("admin.sync")) {
			// Command useless. Will be added in future
			// Will sync all informations from mysql with servers
		} else
			cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_NO_PERMISSIONS));
	}
}
