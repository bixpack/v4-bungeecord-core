package de.bixilon.bixpack.v4.bungeecore.commands;

import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MSG extends Command {
	public MSG() {
		super("msg");
	}

	public void execute(CommandSender cs, String[] args) {
		if (args.length < 2)
			cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_MSG_USAGE));
		else {
			ProxiedPlayer p = Main.getProxiedPlayerByDisplayname(args[0]);
			if (p != null) {
				if (p.getName() != cs.getName()) {
					String message = "";
					for (int i = 1; i < args.length; i++) {
						message += args[i] + " ";
					}
					if (cs instanceof ProxiedPlayer)
						p.sendMessage(Strings.getTextComponent(Strings.COMMAND_MSG_SENT_RECEIVER,
								new String[] { Main.getColoredName((ProxiedPlayer) cs), message }));
					else
						p.sendMessage(Strings.getTextComponent(Strings.COMMAND_MSG_SENT_RECEIVER,
								new String[] { "§4Konsole", message }));
					cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_MSG_SENT_SENDER,
							new String[] { Main.getColoredName(p), message }));
				} else
					cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_MSG_FAIL_SELF));
			} else
				cs.sendMessage(Strings.getTextComponent(Strings.COMMAND_GENERAL_PLAYER_NOT_FOUND));
		}

	}
}
