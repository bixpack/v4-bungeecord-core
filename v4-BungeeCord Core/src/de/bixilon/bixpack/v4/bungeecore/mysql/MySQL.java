package de.bixilon.bixpack.v4.bungeecore.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.UUID;

import de.bixilon.bixpack.v4.bungeecore.classes.Group;
import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MySQL {
	Driver driver;

	public MySQL(Driver d) {
		driver = d;
		AntiDisconnect.setDriver(d);
		Timer timer = new Timer();
		timer.schedule(new AntiDisconnect(), 0, 60000);
	}

	public boolean isConnected() {
		return driver.connected;
	}

	public boolean isPlayerWhitelisted(UUID uuid) {
		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `whitelist` WHERE `uuid` = '" + uuid + "';");

			if (rs.next())
				return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean doesPlayerNeedAuth(ProxiedPlayer p) {
		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery(
					"SELECT `id` FROM `players` WHERE `uuid` = '" + p.getUniqueId() + "' AND `origin` = 1;");

			if (rs.next())
				return false;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	public Player getPlayerByOrigin(ProxiedPlayer p) {

		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `players` WHERE `uuid` = '" + p.getUniqueId() + "' AND `origin` = 1;");

			if (rs.next()) {
				return new Player(p, UUID.fromString(rs.getString("uuid")), rs.getString("username"),
						rs.getInt("group"), rs.getString("token"), rs.getBoolean("origin"), rs.getString("server"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Player getPlayerByUsername(String username) {

		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `players` WHERE `username` = '" + username + "';");

			if (rs.next()) {
				return new Player(null, UUID.fromString(rs.getString("uuid")), rs.getString("username"),
						rs.getInt("group"), rs.getString("token"), rs.getBoolean("origin"), rs.getString("server"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<Group> getGroups() {

		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `groups` WHERE 1;");

			List<Group> ret = new ArrayList<Group>();
			while (rs.next()) {
				ret.add(new Group(rs.getInt("id"), rs.getString("name"), rs.getString("prefix"),
						rs.getString("suffix")));
			}
			return ret;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<String> getPermissions(int group) {

		Statement s = driver.getStatement();
		ResultSet rs;
		try {
			rs = s.executeQuery("SELECT * FROM `permissions` WHERE `group` = " + group + ";");

			List<String> ret = new ArrayList<String>();
			while (rs.next()) {
				ret.add(rs.getString("permission"));
			}
			return ret;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void updatePlayerDetails(ProxiedPlayer p) {

		Statement s = driver.getStatement();
		try {
			if (p.getServer().getInfo() != BungeeCord.getInstance().getServerInfo("auth"))
				s.execute("UPDATE `players` SET `server`='" + p.getServer().getInfo().getName() + "' WHERE `uuid` = '"
						+ p.getUniqueId() + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
