package de.bixilon.bixpack.v4.bungeecore.main;

import net.md_5.bungee.api.chat.TextComponent;

public class Strings {
	public final static int COMMAND_CONSOLE_NO_ALLOWED = 1;
	public final static int COMMAND_PING_USAGE = 2;

	public final static int COMMAND_PING_BUNGEE_SINGLE = 3;
	public final static int COMMAND_PING_BUNGEE_OTHERS = 4;

	public final static int COMMAND_MSG_USAGE = 6;
	public final static int COMMAND_MSG_SENT_SENDER = 7;
	public final static int COMMAND_MSG_SENT_RECEIVER = 8;
	public final static int COMMAND_MSG_FAIL_SELF = 9;

	public final static int COMMAND_KICK_USAGE = 10;
	public final static int COMMAND_KICK_DISCONNECT = 11;
	public final static int COMMAND_KICK_DISCONNECT_REASON = 12;

	public final static int COMMAND_HELP_TEXT = 13;
	public final static int COMMAND_HELP_TEXT_ADMIN = 14;

	public final static int COMMAND_AUTH_USAGE = 15;
	public final static int COMMAND_AUTH_USAGE_ADMIN = 16;
	public final static int COMMAND_AUTH_SUCCESSFUL = 20;
	public final static int COMMAND_AUTH_BY_ADMIN = 23;
	public final static int COMMAND_AUTH_FAILED = 24;
	public final static int COMMAND_AUTH_ALREADY = 25;
	public final static int COMMAND_AUTH_USER_NOT_FOUND = 26;
	public final static int COMMAND_AUTH_ALREADY_VERIFIED = 27;
	public final static int COMMAND_AUTH_TO_ADMIN = 28;
	public final static int COMMAND_AUTH_REJOIN = 30;
	

	public final static int COMMAND_VERSION = 29;

	public final static int JOIN_NO_FORGE = 17;
	public final static int JOIN_NOT_WHITELISTED = 18;
	public final static int JOIN_PLEASE_AUTH = 19;
	public final static int JOIN_PLAYER = 21;
	public final static int QUIT_PLAYER = 22;

	public final static int CHAT_FORMAT = 1000;

	public final static int COMMAND_GENERAL_PLAYER_NOT_FOUND = 100;
	public final static int COMMAND_GENERAL_NO_PERMISSIONS = 101;
	public final static int GENERAL_UNKNOWN_ERROR = 102;

	public static String getString(int name, String a[]) {
		switch (name) {
		case COMMAND_CONSOLE_NO_ALLOWED:
			return "§cLappen";

		case COMMAND_PING_USAGE:
			return "§cBenutzung: §6/ping [Spieler]";
		case COMMAND_PING_BUNGEE_SINGLE:
			return "§3Du hast einen Proxy Ping von §e" + a[0] + "§3ms!";
		case COMMAND_PING_BUNGEE_OTHERS:
			return "§e" + a[1] + " §3hat einen Proxy Ping von §e" + a[0] + "§3ms!";

		case COMMAND_MSG_USAGE:
			return "§cBenutzung: §6/msg <Spieler> <Nachricht>";
		case COMMAND_MSG_SENT_SENDER:
			return "§cDu §6-> §e" + a[0] + "§6: §7" + a[1];
		case COMMAND_MSG_SENT_RECEIVER:
			return "§e" + a[0] + " §6-> §cDir§6: §7" + a[1];
		case COMMAND_MSG_FAIL_SELF:
			return "§cDu kannst keine Selbstgespräche führen!";

		case COMMAND_KICK_USAGE:
			return "§cBenutzung: §6/kick <Spieler> [Grund]!";
		case COMMAND_KICK_DISCONNECT:
			return "§cDu wurdest von §e" + a[0] + " §cgekickt!";
		case COMMAND_KICK_DISCONNECT_REASON:
			return "§cDu wurdest von §e" + a[0] + " §cgekickt!\n\n§3Grund: §e" + a[1];

		case COMMAND_HELP_TEXT:
			return "§c-----------------------------------\n§6/help §7- §6Zeige dieses Menü\n§a/ping [Spieler]§7- §aZeige deinen aktuellen Ping an\n§6/info [Spieler]§7- §aZeige Spielerinfos an\n§a/msg <Spieler> <Nachricht> §7- \n§6Verschicke eine private Nachricht";
		case COMMAND_HELP_TEXT_ADMIN:
			return "§c/kick §7- §cKicke einen Spieler vom Server\n§c/login <Spieler> <Benutzername>";

		case COMMAND_AUTH_USAGE:
			return "§cBenutzung: §6/login <Benutzername> <Token>!";
		case COMMAND_AUTH_USAGE_ADMIN:
			return "§cBenutzung: §6/login <Spieler> <Benutzer>!";
		case COMMAND_AUTH_SUCCESSFUL:
			return "§aWillkommen zurück, " + a[0] + "!\n§aDu hast dich erfolgreich angemeldet";
		case COMMAND_AUTH_BY_ADMIN:
			return "§aEin Administrator hat dich freigeschaltet. Viel Spaß!";
		case COMMAND_AUTH_FAILED:
			return "§cDer eingegebene Token war leider ungültig!\n\n§3Bitte versuche es erneut!";
		case COMMAND_AUTH_ALREADY:
			return "§cDu bist bereits verbunden!";
		case COMMAND_AUTH_ALREADY_VERIFIED:
			return "§cDu bist bereits verifiziert!";
		case COMMAND_AUTH_USER_NOT_FOUND:
			return "§cBenutzername konnte nicht gefunden werden";
		case COMMAND_AUTH_TO_ADMIN:
			return "§aUser wurde freigeschaltet!";
		case COMMAND_AUTH_REJOIN:
			return "§aDu hast dich erfolgreich angemeldet\n\n§3Bitte joine innerhalb §e30 §3Sekunden erneut!";

		case COMMAND_VERSION:
			return "§3Proxy Version: 1.07!";

		case JOIN_NO_FORGE:
			return "§cBitte lade dir das ModPack herunter!";
		case JOIN_NOT_WHITELISTED:
			return "§cUps. Sorry!\n\n§cDu bist leider (noch) kein Teilnehmer des Projektes!\n\n§3Falls du bei BixPackv4 mitspielen möchtest, gehe doch bitte auf §ehttps://bixpack.bixilon.de/v4/apply §3!";
		case JOIN_PLEASE_AUTH:
			return "§cBitte melde dich an!\n§cMach dazu bitte\n§6/login <Benutzername> <Token>\n§cDer Benutzername ist der, den du bei der Registrierung auf der §cWebseite angegeben hast.\n§cDer Token steht auf deinem Handy in der APP §eGoogle Authenticator§c!\n§aAuf gehts!";
		case JOIN_PLAYER:
			return "§6[§a+§6] " + a[0];
		case QUIT_PLAYER:
			return "§6[§4-§6] " + a[0];

		case COMMAND_GENERAL_PLAYER_NOT_FOUND:
			return "§cSpieler nicht gefunden!";
		case COMMAND_GENERAL_NO_PERMISSIONS:
			return "§cDu hast keine Rechte!";
		case GENERAL_UNKNOWN_ERROR:
			return "§4Es ist ein unbekannter Fehler aufgetreten!\n$cBitte melde das dem Team!";

		case CHAT_FORMAT:
			return a[0] + a[1] + " §a» §f" + a[2];

		}
		return "";
	}

	public static String getString(int name) {
		return getString(name, null);
	}

	public static TextComponent getTextComponent(int name) {
		return new TextComponent(getString(name, null));
	}

	public static TextComponent getTextComponent(int name, String a[]) {
		return new TextComponent(getString(name, a));
	}

}
