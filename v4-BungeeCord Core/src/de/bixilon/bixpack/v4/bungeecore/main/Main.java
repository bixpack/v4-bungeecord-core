package de.bixilon.bixpack.v4.bungeecore.main;

import de.bixilon.bixpack.v4.bungeecore.classes.Group;
import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import de.bixilon.bixpack.v4.bungeecore.commands.*;
import de.bixilon.bixpack.v4.bungeecore.events.*;
import de.bixilon.bixpack.v4.bungeecore.mysql.Driver;
import de.bixilon.bixpack.v4.bungeecore.mysql.MySQL;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

public class Main extends Plugin {
	public static MySQL mysql;

	@Override
	public void onEnable() {
		registerCommands();
		registerEvents();

		getLogger().info("§7Connecting to MySQL...");
		mysql = new MySQL(new Driver(Settings.mysql_host, Settings.mysql_port, Settings.mysql_db, Settings.mysql_user,
				Settings.mysql_pw));
		if (mysql.isConnected())
			getLogger().info("§aMySQL connected!");
		else {
			getLogger().warning("§4Connection to MySQL failed!");
			System.exit(2);
		}
		Group.groups = Main.mysql.getGroups();

		getLogger().info("§aAll Data fetched!");

		getLogger().info("§aBungeecore aktiviert!");
	}

	private void registerCommands() {
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Ping());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new MSG());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Kick());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Help());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Sync());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Auth());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Info());
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new Version());
	}

	private void registerEvents() {
		BungeeCord.getInstance().getPluginManager().registerListener(this, new Tablist());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new PostAuth());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new AuthEvent());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new AntiOriginalCommands());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new Leave());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new ChatListener());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new MySQLUpdate());
	}

	public static ProxiedPlayer getProxiedPlayerByDisplayname(String displayname) {

		for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
			if (displayname.equalsIgnoreCase(p.getDisplayName()))
				return p;
		}
		return null;

	}

	public static String getColoredName(ProxiedPlayer p) {
		// Player MUST be authenticated
		Player pi = Main.mysql.getPlayerByUsername(p.getDisplayName());
		if (pi != null) {

			pi.setProxiedPlayer(p);

			return pi.getGroup().getPrefix() + p.getDisplayName() + pi.getGroup().getSuffix();
		}
		return p.getDisplayName();
	}
}