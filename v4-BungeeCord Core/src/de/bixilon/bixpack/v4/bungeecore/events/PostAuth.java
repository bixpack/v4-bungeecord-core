package de.bixilon.bixpack.v4.bungeecore.events;

import java.util.HashMap;
import java.util.UUID;

import de.bixilon.bixpack.v4.bungeecore.classes.AuthInfo;
import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class PostAuth implements Listener {
	public static HashMap<UUID, AuthInfo> auths = new HashMap<UUID, AuthInfo>(); // UUID: Origin UUID

	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PostLoginEvent e) {
		ProxiedPlayer pp = e.getPlayer();
		UUID original = pp.getUniqueId();

		if (auths.containsKey(pp.getUniqueId())) {
			// Tried auth some time. Checking if it was in the last 30secs.....
			AuthInfo ai = auths.get(pp.getUniqueId()); 
			if ((System.currentTimeMillis() / 1000) - ai.getAuthTime() <= 30) {
				// was in last 30secs. Authentication....
				Player p = Main.mysql.getPlayerByUsername(ai.getFakeName());
				p.setProxiedPlayer(pp);

				// Check if player is already connected:
				for (ProxiedPlayer ppp : BungeeCord.getInstance().getPlayers()) {
					if (ppp.getUniqueId().equals(p.getUUID())) {
						// player already there! Kicking both
						ppp.disconnect(Strings.getTextComponent(Strings.COMMAND_AUTH_ALREADY));
						pp.disconnect(Strings.getTextComponent(Strings.COMMAND_AUTH_ALREADY));
						return;
					}
				}

				// setting fake uuid,...
				p.init();
				pp = BungeeCord.getInstance().getPlayer(p.getPlayerName());

				Player.authenticated.add(pp.getUniqueId());
				p.setPermissions();

				p.setAuthentificated(true);

				pp.sendMessage(Strings.getTextComponent(Strings.COMMAND_AUTH_SUCCESSFUL,
						new String[] { Main.getColoredName(pp) }));
				pp.connect(BungeeCord.getInstance().getServerInfo(p.getServer()));
				BungeeCord.getInstance().broadcast(
						Strings.getTextComponent(Strings.JOIN_PLAYER, new String[] { Main.getColoredName(pp) }));

			}
			auths.remove(original);
		}

	}
}
