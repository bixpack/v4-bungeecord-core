package de.bixilon.bixpack.v4.bungeecore.events;

import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Leave implements Listener {

	@EventHandler(priority = 64)
	public void onJoin(PlayerDisconnectEvent e) {
		ProxiedPlayer p = e.getPlayer();

		if (Player.authenticated.contains(p.getUniqueId())) {

			BungeeCord.getInstance()
					.broadcast(Strings.getTextComponent(Strings.QUIT_PLAYER, new String[] { Main.getColoredName(p) }));

			Player.authenticated.remove(p.getUniqueId());
		}
	}
}
