package de.bixilon.bixpack.v4.bungeecore.events;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Tablist implements Listener {

	@EventHandler(priority = 64)
	public void setTabOnServerChange(ServerSwitchEvent e) {
		set(e.getPlayer());
	}

	public void set(ProxiedPlayer p) {

		String server = p.getServer().getInfo().getName();
		String s = "irgendwo";
		switch (server) {
		case "auth":
			s = "beim Anmelden";
			break;
		case "dorf":
			s = "im Dorf";
			break;
		case "farm-1":
			s = "in der 1. Farwelt";
			break;
		case "farm-2":
			s = "in der 2. Farwelt";
			break;
		}

		BaseComponent[] first = new ComponentBuilder(
				"§f« §aBixPack§ev4 §f»\n§3Dieses Projekt ist eine Zusammenarbeit zwischen §aBixilon §3und dem §6Cafe-Netzwerk§3!")
						.create();
		BaseComponent[] second = new ComponentBuilder(
				"§7Du brauchst Hilfe? §c/help§7\nDer TeamSpeak-Server » §6bixilon.de\n\n§6Du bist aktuell " + s + "!")
						.create();
		p.setTabHeader(first, second);
	}

}
