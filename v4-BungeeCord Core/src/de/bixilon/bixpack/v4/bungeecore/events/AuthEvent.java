package de.bixilon.bixpack.v4.bungeecore.events;

import de.bixilon.bixpack.v4.bungeecore.classes.Player;
import de.bixilon.bixpack.v4.bungeecore.main.Main;
import de.bixilon.bixpack.v4.bungeecore.main.Strings;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class AuthEvent implements Listener {
	@EventHandler(priority = 64)
	public void onJoin(ServerConnectEvent e) {
		ProxiedPlayer pp = e.getPlayer();
		if (!pp.getName().endsWith("_f"))
			if (!Main.mysql.isPlayerWhitelisted(pp.getUniqueId())) {
				// Ups. Sorry

				pp.disconnect(Strings.getTextComponent(Strings.JOIN_NOT_WHITELISTED));
				return;
			}

		if (!Player.authenticated.contains(pp.getUniqueId())) {

			if (Main.mysql.doesPlayerNeedAuth(pp)) {
				// Must Authentificate player
				e.setTarget(BungeeCord.getInstance().getServerInfo("auth"));
				pp.sendMessage(Strings.getTextComponent(Strings.JOIN_PLEASE_AUTH));
				Title t = BungeeCord.getInstance().createTitle();
				t.title(new ComponentBuilder("§aWillkommen").create());
				t.subTitle(new ComponentBuilder("§cBitte melde dich mit §6/login§c an!").create());
				t.send(pp);
			} else {
				// is an origin Minecraft Player
				Player p = Main.mysql.getPlayerByOrigin(pp);
				p.setProxiedPlayer(pp);
				p.setAuthentificated(true);
				if (p.getServer() == "auth")
					e.setTarget(BungeeCord.getInstance().getServerInfo("dorf"));
				else
					e.setTarget(BungeeCord.getInstance().getServerInfo(p.getServer()));
				Player.authenticated.add(pp.getUniqueId());
				p.setPermissions();
				BungeeCord.getInstance().broadcast(
						Strings.getTextComponent(Strings.JOIN_PLAYER, new String[] { Main.getColoredName(pp) }));
				//p.getGroup().scoreboadteam.addPlayer(p.getPlayerName());
				//ScoreBoard.init();
			}
		}

	}
}
