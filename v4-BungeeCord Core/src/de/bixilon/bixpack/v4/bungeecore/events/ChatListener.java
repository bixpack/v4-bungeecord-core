package de.bixilon.bixpack.v4.bungeecore.events;

import de.bixilon.bixpack.v4.bungeecore.classes.Chat;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener implements Listener {

	@EventHandler(priority = 64)
	public void onJoin(ChatEvent e) {

		if (!e.getMessage().startsWith("/")) {
			Chat.chat((ProxiedPlayer) e.getSender(), e.getMessage());
			e.setCancelled(true);
		}

	}
}
