package de.bixilon.bixpack.v4.bungeecore.events;

import de.bixilon.bixpack.v4.bungeecore.main.Main;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class MySQLUpdate implements Listener {

	@EventHandler(priority = 64)
	public void onJoin(ServerSwitchEvent e) {
		// Player update data
		Main.mysql.updatePlayerDetails(e.getPlayer());

	}
}
