package de.bixilon.bixpack.v4.bungeecore.events;

import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class AntiOriginalCommands implements Listener {

	@EventHandler(priority = 64)
	public void onJoin(ChatEvent e) {

		String m = e.getMessage();

		if (m.startsWith("/")) {
			boolean cancel = false;
			m = m.substring(1, m.length());

			if (m.startsWith("minecraft") || m.startsWith("bukkit"))
				cancel = true;

			if (cancel)
				e.setCancelled(true);
		}

	}
}
